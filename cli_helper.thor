class CliHelper < Thor
  include Thor::Actions

  desc "set_proxy", "Set a proxy url on the given CLI"
  method_option :cli, type: :string, desc: 'Which CLI do you want to update? Allowed values: [npm]'
  method_option :proxy_url, type: :string, desc: 'The proxy url'
  def set_proxy
    case options[:cli]
    when 'npm'
      system *%W(npm config set proxy #{options[:proxy_url]})
      system *%W(npm config set https-proxy #{options[:proxy_url]})
      say('Proxy set.', :green)
    end
  end

  desc "unset_proxy", "Unset a proxy url on the given CLI"
  def unset_proxy
    system *%W(npm config delete proxy)
    system *%W(npm config delete https-proxy)
    say('Proxy unset.', :green)
  end
end
